# TU Wien - Business Informatics Group


#### Inhaltsverzeichnis

* [Aufbereitung von Inhalten](documentation/content.md)
* [Technische Details und Wartung](documentation/tech.md)
